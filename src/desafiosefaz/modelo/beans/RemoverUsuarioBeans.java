package desafiosefaz.modelo.beans;

import javax.faces.bean.ManagedBean;

import desafiosefaz.controlador.fachada.FachadaControlador;
import desafiosefaz.controlador.usuario.SessionContext;

@ManagedBean
public class RemoverUsuarioBeans {

	public String excluirUsuario() {

		String id = SessionContext.getInstance().getParametroId("idUsuario");
		int idUsuario = Integer.parseInt(id);

		FachadaControlador.getInstanciaUsuario().removerUsuario(idUsuario);

		return null;
	}
}
