package desafiosefaz.modelo.beans;

import java.util.Date;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;

import desafiosefaz.controlador.fachada.FachadaControlador;
import desafiosefaz.modelo.usuario.Usuario;

@ManagedBean
public class CadastrarUsuarioBeans {
	@EJB
	private Usuario usuario;

	public Usuario getUsuario() {
		if(usuario == null) {
			usuario = new Usuario ();
		}
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String cadastrarUsuario() {
		Date dataAtual = new Date();
		usuario.setData(dataAtual);
		FachadaControlador.getInstanciaUsuario().cadastrarUsuario(usuario);

		usuario = new Usuario();
		return "principal.xhtml?faces-redirect=true";
	}
	public String cadastrarInicial() {
		Date dataAtual = new Date();
		usuario.setData(dataAtual);
		FachadaControlador.getInstanciaUsuario().cadastrarUsuario(usuario);

		usuario = new Usuario();
		return "login.xhtml?faces-redirect=true";
	}
}
