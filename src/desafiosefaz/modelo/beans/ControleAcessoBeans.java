package desafiosefaz.modelo.beans;

import java.util.Date;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import desafiosefaz.controlador.fachada.FachadaControlador;
import desafiosefaz.controlador.usuario.SessionContext;
import desafiosefaz.modelo.usuario.Usuario;

@ManagedBean
@SessionScoped
public class ControleAcessoBeans {

	private Usuario usuario;

	public Usuario getUsuario() {
		if (this.usuario == null) {
			this.usuario = new Usuario();
		}
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String logar() {

		Usuario logar = FachadaControlador.getInstanciaUsuario().loginAcesso(usuario.getEmail(), usuario.getSenha());
		if (logar != null) {

			SessionContext.getInstance().setAttribute("usuariologado", usuario);
			System.out.println("Logado com sucesso");
			return "/principal/principal.xhtml?faces-redirect=true";
		}
		return "login.xhtml?faces-redirect=true";
	}

	public String logout() {

		SessionContext.getInstance().encerrarSessao();
		return "/login.xhtml?faces-redirect=true";
	}

	public String cadastroInicial() {
		if (usuario != null) {
			Date dataAtual = new Date();
			usuario.setData(dataAtual);
			FachadaControlador.getInstanciaUsuario().cadastrarUsuario(usuario);
			return "/login.xhtml?faces-redirect=true";
		}

		return "/cadastroInicial.xhtml?faces-redirect=true";
	}

}
