package desafiosefaz.modelo.usuario;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import entityManagerJPA.EntityManagerUtil;

public class UsuarioRepositorio {
	EntityManager em = EntityManagerUtil.getEntityManager();

	public void cadastrarUsuario(Usuario u) {
		em.getTransaction().begin();
		em.persist(u);
		em.getTransaction().commit();

	}

	public Usuario loginAcesso(String email, String senha) {
		try {
			Usuario usuario = (Usuario) em
					.createQuery("SELECT usuario from Usuario usuario where usuario.email= \r\n"
							+ "	             :email and usuario.senha = :senha")
					.setParameter("email", email).setParameter("senha", senha).getSingleResult();

			return usuario;
		} catch (NoResultException e) {
			return null;
		}

	}

	public void removerUsuario(int idUsuario) {
		Usuario usuario = em.find(Usuario.class, idUsuario);
		em.getTransaction().begin();
		em.remove(usuario);
		em.getTransaction().commit();
	}

	public void atualizarUsuario(Usuario u) {
		em.getTransaction().begin();
		em.merge(u);
		em.getTransaction().commit();
	}

	public List<Usuario> listarUsuarios() {
		String consulta = "From Usuario";
		TypedQuery<Usuario> query = em.createQuery(consulta, Usuario.class);
		List<Usuario> resultado = query.getResultList();

		return resultado;
	}

}
