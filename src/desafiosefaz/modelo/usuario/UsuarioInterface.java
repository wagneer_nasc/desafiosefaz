package desafiosefaz.modelo.usuario;

import java.util.List;

public interface UsuarioInterface {
	public void cadastrarUsuario(Usuario u);

	public Usuario loginAcesso(String email, String senha);

	public void removerUsuario(int idUsuario);

	public void atualizarUsuario(Usuario u);

	public List<Usuario> listarUsuarios();

}
