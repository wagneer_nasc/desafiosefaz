package desafiosefaz.modelo.fachada;

import java.util.List;

import desafiosefaz.modelo.usuario.Usuario;
import desafiosefaz.modelo.usuario.UsuarioInterface;
import desafiosefaz.modelo.usuario.UsuarioRepositorio;

public class FachadaRepositorio implements UsuarioInterface {

	private static UsuarioRepositorio usuarioInstancia;

	public FachadaRepositorio() {
		usuarioInstancia = new UsuarioRepositorio();
	}

	public static UsuarioRepositorio getInstanciaUsuario() {
		if (usuarioInstancia == null) {
			usuarioInstancia = new UsuarioRepositorio();
		}
		return usuarioInstancia;
	}

	@Override
	public void cadastrarUsuario(Usuario u) {
		usuarioInstancia.cadastrarUsuario(u);
	}

	@Override
	public Usuario loginAcesso(String email, String senha) {
		return usuarioInstancia.loginAcesso(email, senha);
	}

	@Override
	public void removerUsuario(int idUsuario) {
		usuarioInstancia.removerUsuario(idUsuario);
	}


	@Override
	public List<Usuario> listarUsuarios() {
		return usuarioInstancia.listarUsuarios();
	}

	@Override
	public void atualizarUsuario(Usuario u) {
		usuarioInstancia.atualizarUsuario(u);
		
	}

}
