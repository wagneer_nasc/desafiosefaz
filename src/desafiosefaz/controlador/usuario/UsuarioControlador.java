package desafiosefaz.controlador.usuario;

import java.util.List;

import desafiosefaz.modelo.fachada.FachadaRepositorio;
import desafiosefaz.modelo.usuario.Usuario;
import desafiosefaz.modelo.usuario.UsuarioInterface;

public class UsuarioControlador implements UsuarioInterface {

	private static FachadaRepositorio repositorio;

	public UsuarioControlador() {
		repositorio = new FachadaRepositorio();
	}

	@Override
	public void cadastrarUsuario(Usuario u) {
		repositorio.cadastrarUsuario(u);
	}

	@Override
	public Usuario loginAcesso(String email, String senha) {
		return repositorio.loginAcesso(email, senha);
	}

	@Override
	public void removerUsuario(int idUsuario) {
		repositorio.removerUsuario(idUsuario);
	}

	@Override
	public void atualizarUsuario(Usuario u) {
		repositorio.atualizarUsuario(u);
	}

	@Override
	public List<Usuario> listarUsuarios() {
		return repositorio.listarUsuarios();
	}

}
