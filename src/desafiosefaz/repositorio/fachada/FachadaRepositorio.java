package desafiosefaz.repositorio.fachada;

import desafiosefaz.modelo.usuario.Usuario;
import desafiosefaz.modelo.usuario.UsuarioInterface;
import desafiosefaz.modelo.usuario.UsuarioRepositorio;

public class FachadaRepositorio implements UsuarioInterface {

	private static UsuarioRepositorio usuarioInstancia;

	public FachadaRepositorio() {
		usuarioInstancia = new UsuarioRepositorio();
	}

	public static UsuarioRepositorio getInstanciaSalario() {
		if (usuarioInstancia == null) {
			usuarioInstancia = new UsuarioRepositorio();
		}
		return usuarioInstancia;
	}
	public void cadastrarUsuario(Usuario u) {
		usuarioInstancia.cadastrarUsuario(u);

	}
	public Usuario loginAcesso(String email, String senha) {
		// TODO Auto-generated method stub
		return usuarioInstancia.loginAcesso(email, senha);
	}

}
